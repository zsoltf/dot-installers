#! /bin/bash

clear
echo
echo '__/\\\________/\\\_____________________________'
echo ' _\/\\\_______\/\\\_____________________________'
echo '  _\//\\\______/\\\____/\\\______________________'
echo '   __\//\\\____/\\\____\///______/\\\\\__/\\\\\___'
echo '    ___\//\\\__/\\\______/\\\___/\\\///\\\\\///\\\_'
echo '     ____\//\\\/\\\______\/\\\__\/\\\_\//\\\__\/\\\_'
echo '      _____\//\\\\\_______\/\\\__\/\\\__\/\\\__\/\\\_'
echo '       ______\//\\\________\/\\\__\/\\\__\/\\\__\/\\\_'
echo '        _______\///_________\///___\///___\///___\///__'
echo



# Vim for Programming
#####################
#
# Automated installer for a customized version of vim.
# Ubuntu version. Includes ruby, python, lua and dotfiles.
# Logs all command output to file.
#
# Steps:
# 1. installs dependencies
# 2. clones, configures, builds and installs vim
# 3. clones and configures dotfiles


DOTFILES_REPO=https://github.com/zsoltf/dot-vim.git


hello() {
  touch make-vim.log
  echo Vim for Programming
  echo -e "Vim for Programming\n" >> make-vim.log
  echo -------------------
  echo
}

check_if_root() {
  if [[ $EUID -ne 0 ]]; then
    echo "You must be a root" 2>&1
    exit 1
  fi
}

dependencies() {
  echo -n "Installing dependencies..."
  echo -e "Installing dependencies...\n" >> make-vim.log
  apt-get install -y libncurses5-dev python-dev ruby-dev \
    lua5.1 luajit libluajit-5.1 liblua5.1-dev libtolua-dev \
    mercurial git exuberant-ctags curl >> make-vim.log 2>&1
  # install powerline fonts
  if [ ! -d ~/.fonts ]
  then
    wget -q https://github.com/Lokaltog/powerline-fonts/archive/master.zip
    unzip -q master.zip
    mkdir ~/.fonts
    IFS=$'\n'; for f in $(ls -1R powerline-fonts-master/*/*.{otf,ttf}); do
      cp "$f" ~/.fonts/
    done
    rm -rf master.zip
    rm -rf powerline-fonts-master/
  fi
  echo "done."
}

clone() {
  mkdir -p ./vim
  echo -n "Cloning Vim..."
  echo -e "\nCloning Vim...\n" >> make-vim.log
  git clone https://github.com/vim/vim ./vim >> make-vim.log 2>&1
  echo "done."
}

configure() {
  [ -d ./vim ] || clone
  echo -n "Configuring..."
  echo -e "\nConfiguring...\n" >> make-vim.log
  config_log=$(cd ./vim && ./configure \
    --with-features=huge \
    --enable-multibyte \
    --enable-rubyinterp \
    --enable-pythoninterp \
    --enable-luainterp \
    --with-luajit \
    --enable-cscope \
    --with-compiledby="Zsolt Fekete (zsoltf@icloud.com)" 2>&1)
  echo "$config_log" >> make-vim.log
  echo "done."
}

install() {
  echo -n "Building..."
  echo -e "\nBuilding...\n" >> make-vim.log
  make_log=$(cd vim && make 2>&1)
  echo "$make_log" >> make-vim.log
  echo "done."

  echo -n "Installing..."
  echo -e "\nInstalling...\n" >> make-vim.log
  install_log=$(cd vim && make install 2>&1)
  echo "$install_log" >> make-vim.log
  echo "done."
}

dotfiles() {
  echo -n "Plugins..."
  echo -e "\nPlugins...\n" >> make-vim.log
  if ! [ -d ~/.vim ]
  then
    mkdir -p ~/.vim
    dot_log=$(git clone $DOTFILES_REPO ~/.vim 2>&1)
    echo "$dot_log" >> make-vim.log
    dot_log=$(cd ~/.vim && git submodule init && git submodule update 2>&1 >> make-vim.log)
    echo >> make-vim.log
    echo "$dot_log" >> make-vim.log
  fi
  echo -e "\nCreating vimrc..." >> make-vim.log
  [ -f ~/.vimrc ] || echo "source ~/.vim/vimrc" >> ~/.vimrc
  echo -e "\nInstalling Plugins..." >> make-vim.log
  vim -s ~/.vimrc +BundleInstall +qall
  echo "Plugins...done."
}

update_permissions() {
  echo -n "Updating Permissions..."
  echo -e "\nUpdating Permissions...\n" >> make-vim.log
  chown -R $SUDO_USER:`id -g $SUDO_USER` ~/.vimrc >> make-vim.log
  chown -R $SUDO_USER:`id -g $SUDO_USER` ~/.vim/ >> make-vim.log
  chown -R $SUDO_USER:`id -g $SUDO_USER` ~/.viminfo >> make-vim.log
  echo "done."
}

cleanup() {
  today=`date +%F`
  mv make-vim.log "make-vim-$today.log"
  rm -rf ./vim
}


#############
# main
#############

check_if_root
hello
dependencies
configure
install
dotfiles
update_permissions
cleanup

#############
