#!/bin/bash

# Ruby Environment
##################
#
# Install Ruby the right® way
#
# Inspired by the excellent guide from Beesbot:
# http://www.beesbot.com/rbenv-ubuntu-bundler/

RUBY_VERSION='2.1.2'
RAILS_VERSION='' # latest

header(){
  echo
  echo '  ____ ____ ____ ____ _________ ____ ____ ____'
  echo ' ||R |||u |||b |||y |||       |||E |||n |||v ||'
  echo ' ||__|||__|||__|||__|||_______|||__|||__|||__||'
  echo ' |/__\|/__\|/__\|/__\|/_______\|/__\|/__\|/__\|'
  echo
}


hello() {
  clear
  touch make-rbenv.log
  header
  header >> make-rbenv.log
  echo Ruby Environment
  echo -e "Ruby Environment\n" >>make-rbenv.log
  echo ----------------
  echo
}

clone() {
  echo -ne "Cloning RBenv and ruby-build..."
  echo -e "\nCloning RBenv and ruby-build...\n" >> make-rbenv.log

  if [ ! -d ~/.rbenv ]
  then
    git clone https://github.com/sstephenson/rbenv.git ~/.rbenv >> make-rbenv.log 2>&1
    git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build >> make-rbenv.log 2>&1
  fi

  echo "done."
}

config_files() {
  echo -ne "Adding RBenv to bashrc..."
  echo -e "\nAdding RBenv to bashrc...\n" >> make-rbenv.log

  if grep -q "rbenv" ~/.bashrc
  then
    echo 'done.'
  else
    echo '# rbenv' >> ~/.bashrc
    echo 'export PATH="$PATH:$HOME/.rbenv/bin"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    echo "done."
  fi
  grep "rbenv" ~/.bashrc >> make-rbenv.log
  echo >> make-rbenv.log
}

dependencies() {
  echo -ne "Installing dependencies...\n"
  echo -e "Installing dependencies...\n" >> make-rbenv.log

  sudo apt-get install build-essential libreadline-dev libssl-dev zlib1g-dev libsqlite3-dev nodejs libxslt-dev libxml2-dev libpq-dev postgresql imagemagick -y >> make-rbenv.log 2>&1

  echo "Installing dependencies...done."
}

build_ruby() {
  rbenv install -f $RUBY_VERSION | tee -a make-rbenv.log
  rbenv global $RUBY_VERSION | tee -a make-rbenv.log
  echo -n "Ruby installed successfully: " | tee -a make-rbenv.log
  echo "$(rbenv which ruby)" >> make-rbenv.log
  echo "$(ruby -v)" | tee -a make-rbenv.log
}

install_rails() {
  echo -ne "Installing Rails..."
  echo -e "\nInstalling Rails...\n" >> make-rbenv.log

  gem install rails $RAILS_VERSION >> make-rbenv.log
  rbenv rehash
  gem install bundler >> make-rbenv.log
  rbenv rehash

  echo "done."
}

bundler_aliases() {
  echo
  echo '# bundler'
  echo 'alias b="bundle"'
  echo 'alias bi="b install --path vendor"'
  echo 'alias bil="bi --local"'
  echo 'alias bu="b update"'
  echo 'alias be="b exec"'
  echo 'alias binit="bi && b package && echo 'vendor/ruby' >> .gitignore"'
  echo 'alias ber="be rspec"'
  echo 'alias berp="be rake db:test:prepare && ber"'
}

set_aliases() {
  echo -ne "Setting Aliases..."
  echo -ne "\nSetting Aliases..." >> make-rbenv.log
  if grep -q "b install --path vendor" ~/.bashrc
  then
    echo "done." | tee -a make-rbenv.log
  else
    bundler_aliases >> ~/.bashrc
    echo "done." | tee -a make-rbenv.log
  fi
}

cleanup() {
  today=`date +%F`
  mv make-rbenv.log "make-rbenv-$today.log"
  echo
}

# main

hello
dependencies
clone
config_files
build_ruby
install_rails
set_aliases
cleanup
